import java.util.Arrays;
import java.util.List;

public class Data {

    static String matrix[][] = {
            {"A", "L", "O", "N", "E"},
            {"S", "A", "D", "A", "B"},
            {"H", "A", "P", "P", "Y"},
            {"E", "U", "T", "Q", "W"},
            {"S", "O", "C", "O", "L"},
    };

    static List<String> englishWords = Arrays.asList("ALONE", "HAPPY", "ASHES");

}
