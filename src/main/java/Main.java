import java.util.List;

public class Main {

    public static void main(String[] args) {

        //WORDS BUILDER
        InputProvider inputProvider = new InputProvider(Data.matrix);
        inputProvider.provideInputAndCompareToTheMatrix();
        inputProvider.askAgain();

        //FIND WORDS
        FindWords findWords = new FindWords(Data.matrix);
        List<String> wordsFromMatrix = findWords.collectAllWords();
        findWords.checkIfMatrixContainsEnglishWords(wordsFromMatrix, Data.englishWords);

        //SUM
        int numbersToFindExpectedValue[] = {9, 7, 19, 2, 1, 4};
        int expectedSum = 10;

        MathOperations mathOperations = new MathOperations();
        mathOperations.findSumOfPairGivesExpectedValue(numbersToFindExpectedValue, expectedSum);

        //ARRAY
        int numbersToFindSum[] = {1, 3, 7, 3, 5};
        mathOperations.findSumOffPairGivesThirdElementPlusOne(numbersToFindSum, numbersToFindSum.length);
    }
}
