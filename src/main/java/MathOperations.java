import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class MathOperations {

    private Logger logger = LoggerFactory.getLogger(InputProvider.class);

    public boolean findSumOfPairGivesExpectedValue(int[] numbersToAdd, int expectedSum) {
        Arrays.sort(numbersToAdd);
        for (int i = 0, j = numbersToAdd.length - 1; i < j; ) {
            int sum = numbersToAdd[i] + numbersToAdd[j];
            if (sum < expectedSum)
                i++;
            else if (sum > expectedSum)
                j--;
            else {
                logger.info("Pair of elements: '" + numbersToAdd[i] + "' and '" + numbersToAdd[j] + "' gives expected sum: " + expectedSum);
                return true;
            }
        }
        logger.info("There was no pairs to gives sum: " + expectedSum);
        return false;
    }

    public void findSumOffPairGivesThirdElementPlusOne(int arr[], int n) {

        Arrays.sort(arr);

        for (int i = n - 1; i >= 0; i--) {
            int j = 0;
            int k = i - 1;

            while (j < k) {
                if ((arr[i] + 1) == arr[j] + arr[k]) {
                    // pair found
                    logger.info("The sum of two array elements: '" + arr[j] + "' and '" + arr[k] + "' gives a sum of third element plus one " + (arr[i] + 1));
                    return;
                } else if ((arr[i] + 1) > arr[j] + arr[k])
                    j += 1;
                else
                    k -= 1;
            }
        }
        // no such pair
        System.out.println("No such triplet exists");
    }

}

