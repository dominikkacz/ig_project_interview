import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class FindWords {

    private Logger logger = LoggerFactory.getLogger(FindWords.class);
    private String[][] matrix;

    FindWords(String[][] matrix) {
        this.matrix = matrix;
    }

    public List<String> collectFromTopToDownMatrix() {
        List<String> wordsFromTopToDown = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            String word = "";
            for (int j = 4; j >= 0; j--) {
                word = matrix[i][j] + word;
            }
            wordsFromTopToDown.add(word);
        }
        logger.info("Words collected from matrix from top do the down: " + wordsFromTopToDown);
        return wordsFromTopToDown;
    }

    List<String> collectFromLeftToRight() {
        List<String> wordsFromLeftToRight = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            String word = "";
            for (String[] character :
                    matrix
            ) {
                word = word + character[i];
            }
            wordsFromLeftToRight.add(word);
        }
        logger.info("Words collected from matrix from right to the left: " + wordsFromLeftToRight);
        return wordsFromLeftToRight;
    }

    List<String> collectAllWords() {
        List<String> allWords = new ArrayList<>();
        allWords.addAll(collectFromTopToDownMatrix());
        allWords.addAll(collectFromLeftToRight());
        return allWords;
    }

    public boolean checkIfMatrixContainsEnglishWords(List<String> wordsFromMatrix, List<String> englishWords) {
        boolean matrixContainsEnglishWords = wordsFromMatrix.containsAll(englishWords);
        if (matrixContainsEnglishWords)
            logger.info("Words from matrix: " + wordsFromMatrix + " contains following english words: " + englishWords);
        else
            logger.info("Words from matrix: " + wordsFromMatrix + " does not contain following english words: " + englishWords);
        return matrixContainsEnglishWords;
    }
}
