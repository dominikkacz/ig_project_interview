import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class InputProvider {

    private Logger logger = LoggerFactory.getLogger(InputProvider.class);
    private String[][] matrix;
    private Scanner scanner = new Scanner(System.in);

    InputProvider(String[][] matrix) {
        this.matrix = matrix;
    }

    public boolean provideInputAndCompareToTheMatrix() {
        Boolean inputCouldBeConstructedByMatrix;
        logger.info("Welcome to 'WORDS BUILDER' program!\n Please provide a word:");
        String input = scanner.next();
        logger.info("A word: '" + input + "' is provided to the system");
        inputCouldBeConstructedByMatrix = compareInputToMatrix(input);
        return inputCouldBeConstructedByMatrix;
    }

    public boolean compareInputToMatrix(String input) {
        List<String> inputToList = Arrays.asList(input.split(""));
        List<String> collection = Arrays.stream(matrix).flatMap(Arrays::stream).collect(Collectors.toList());
        if (collection.containsAll(inputToList)) {
            logger.info("'" + input + "' input word can be constructed by matrix.  Result: success\n");
        } else
            logger.info("'" + input + "' input word can not be constructed by matrix.  Result: failed\n");
        return collection.containsAll(inputToList);
    }

    public void askAgain() {
        logger.info("Would you like to provide another word? Press any key to continue or 'N' to exit");
        String inputYN = scanner.next();
        if (!inputYN.equals("N")) {
            provideInputAndCompareToTheMatrix();
            askAgain();
        }
    }


}
