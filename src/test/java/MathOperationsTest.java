import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MathOperationsTest {

    MathOperations mathOperations = new MathOperations();

    private int validNumbersToAdd[] = {9, 7, 19, 2, 4, 8};
    private int invalidNumbersToAdd[] = {9, 7, 19, 2, 11, 4};
    private int expectedSum = 10;


    @Test
    void shouldReturnTrueWhenPairGivesExpectedSum() {
        assertThat(mathOperations.findSumOfPairGivesExpectedValue(validNumbersToAdd, expectedSum))
                .as("At least one of pairs from array: " + validNumbersToAdd + " should returns sum: " + expectedSum
                        + " but it is not").isEqualTo(true);
    }

    @Test
    void shouldReturnFalseWhenPairDoesNotGiveExpectedSum() {
        assertThat(mathOperations.findSumOfPairGivesExpectedValue(invalidNumbersToAdd, expectedSum))
                .as("At least one of pairs from array: " + invalidNumbersToAdd + " does not should returns sum: " + expectedSum
                        + " but it does").isEqualTo(false);
    }
}
